###############################################################################
# Please add "$(call inherit-product, vendor/partner_gms/products/turbo.mk)"
# to your product makefile to integrate Turbo app.
#
PRODUCT_PACKAGES += Turbo

# Overlay for Turbo
PRODUCT_PACKAGE_OVERLAYS += vendor/partner_gms/overlay/gms_overlay_turbo
