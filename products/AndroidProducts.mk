PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/aosp_bramble_gms.mk \
    $(LOCAL_DIR)/aosp_brambleg_64.mk \
    $(LOCAL_DIR)/aosp_cf_x86_64_phone_gms.mk

COMMON_LUNCH_CHOICES := \
    aosp_bramble_gms-userdebug \
    aosp_brambleg_64-userdebug \
    aosp_cf_x86_64_phone_gms-userdebug
