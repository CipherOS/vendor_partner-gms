###############################################################################
# WebView Chromium
LOCAL_PATH := $(my-dir)

my_archs := arm arm64 x86 x86_64
my_src_arch := $(call get-prebuilt-src-arch, $(my_archs))

ifeq ($(TARGET_SUPPORTS_32_BIT_APPS),true)
include $(CLEAR_VARS)
LOCAL_MODULE := WebViewGoogle
LOCAL_MODULE_OWNER := google
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
#LOCAL_PRIVILEGED_MODULE :=
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_SRC_FILES := $(LOCAL_MODULE)_$(my_src_arch).apk
LOCAL_OVERRIDES_PACKAGES := webview
LOCAL_REQUIRED_MODULES := \
    TrichromeLibrary \
    libwebviewchromium_loader \
    libwebviewchromium_plat_support
LOCAL_MULTILIB := both
include $(BUILD_PREBUILT)
endif

ifeq ($(TARGET_SUPPORTS_32_BIT_APPS),false)
include $(CLEAR_VARS)
LOCAL_MODULE := WebViewGoogle64
LOCAL_MODULE_OWNER := google
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
#LOCAL_PRIVILEGED_MODULE :=
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_SRC_FILES := $(LOCAL_MODULE)_$(my_src_arch).apk
LOCAL_OVERRIDES_PACKAGES := webview WebViewGoogle
LOCAL_REQUIRED_MODULES := \
    TrichromeLibrary64 \
    libwebviewchromium_loader \
    libwebviewchromium_plat_support
LOCAL_MULTILIB := 64
include $(BUILD_PREBUILT)
endif
